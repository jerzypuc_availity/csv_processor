# csv_processor
Since the language for this exercise wasn't specified I decided to write it in bash.

## System requirements
script uses MIT licensed csvkit which needs to be installed before execution
https://csvkit.readthedocs.io/en/1.0.5/tutorial/1_getting_started.html#installing-csvkit

on Mac, you can install it using:
```
brew install csvkit
``` 

## Running
You can run the script 
```
run.sh <your_file.csv>
```
after it completes, you will find set of files for each insurance in files `final_[InsuranceName].csv`

## Assumptions
- the csv input file has no headers
- input csv has four columns specified in order or documentation:
  - User Id (string)
  - First and Last Name (string)
  - Version (integer)
  - Insurance Company (string)
- Full Name consist of any number of first names following by last name. Last name is always last word in the string.

