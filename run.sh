#!/bin/bash

rm -f *.tmp.csv

csvcut -c 4 $1 | sort | uniq | while read file
do
    echo -n "Processing insurance \"$file\" "
    csvgrep -H -c 4 -m "$file" $1 | tail -n +2 > "ins_$file.tmp.csv"
    echo -n "."
    csvsql -H --query "select a,b,cast(max(c) as integer),d from 'ins_$file.tmp' group by a order by replace(b, rtrim(b, replace(b, ' ', '')), ''),rtrim(b, replace(b, ' ', '')) ASC" "ins_$file.tmp.csv" | tail -n +2 > "final_$file.csv"
    echo -n "."
    rm -f "ins_$file.tmp.csv"
    echo -n "."
    echo " Done!"
done


